import React, {useEffect, useRef, useState} from 'react';
import './Chrono.css';
import {clearInterval, setInterval} from 'timers';

function Chrono() {
    const [startTime, setStartTime] = useState<Date | null>(null);
    const [stopTime, setStopTime] = useState<Date | null>(null);
    const [diffInMillis, setDiffInMillis] = useState<number>(0);
    const interval = useRef<NodeJS.Timeout>();

    useEffect(
        () => {
            function calculate(): void {
                let last = new Date();
                if (startTime === null) {
                    setDiffInMillis(0);
                    return;
                } else if (stopTime !== null) {
                    last = stopTime
                }
                setDiffInMillis(last.getTime() - startTime.getTime());
            }

            interval.current = setInterval(calculate, 5);

            return () => {
                stop();
            };
        }
    );

    function stop() {
        if (interval.current !== undefined)
            clearInterval(interval.current)
    }

    return (
        <div className="Chrono">
            <span className="chronometer">{show(diffInMillis)}</span>
            <div className="control">
                <button className="start" onClick={() => {
                    setStartTime(new Date())
                }} disabled={startTime !== null}>START
                </button>
                <button className="stop" onClick={() => {
                    setStopTime(new Date());
                    stop();
                }} disabled={stopTime !== null || startTime === null}>STOP
                </button>
                <button onClick={() => {
                    setStartTime(null);
                    setStopTime(null);
                    stop();
                }}>RESET
                </button>
            </div>
        </div>
    );
}

function format(n: number, digit: number) {
    let result = "";
    for (let i = 0; i < digit - n.toString().length; i++)
        result += "0";
    result += n.toString();
    return result;
}

function show(timeInMillis: number) {
    const diff: Date = new Date(0);
    diff.setTime(timeInMillis);
    return format(diff.getUTCHours(), 2) + ":" + format(diff.getUTCMinutes(), 2) + ":" + format(diff.getUTCSeconds(), 2) + "." + format(diff.getUTCMilliseconds(), 3);
}

export default Chrono;