import React from 'react';
import {render} from '@testing-library/react';
import Chrono from './Chrono';

test('init chronometer', () => {
    const {getByText} = render(<Chrono/>);
    const linkElement = getByText(/00:00:00.000/i);
    expect(linkElement).toBeInTheDocument();
});

test('init check buttons', () => {
    const {getByText} = render(<Chrono/>);
    const stopButton = getByText(/STOP/i).closest("button");
    expect(stopButton).toBeDisabled();
    const startButton = getByText(/START/i).closest("button");
    expect(startButton).toBeEnabled();
    const resetButton = getByText(/RESET/i).closest("button");
    expect(resetButton).toBeEnabled();
});

test('start chronometer', () => {
    const {getByText} = render(<Chrono/>);
    const startButton = getByText(/START/i).closest("button");
    const stopButton = getByText(/STOP/i).closest("button");
    if (startButton !== null) {
        startButton.click();
        expect(startButton).toBeDisabled();
        expect(stopButton).toBeEnabled();
    }
});

test('stop chronometer', () => {
    const {getByText} = render(<Chrono/>);
    const startButton = getByText(/START/i).closest("button");
    const stopButton = getByText(/STOP/i).closest("button");
    if (startButton !== null)
        startButton.click();
    if (stopButton !== null) {
        stopButton.click();
        expect(stopButton).toBeDisabled();
        expect(startButton).toBeDisabled();
    }
});

test('reset chronometer 1', () => {
    const {getByText} = render(<Chrono/>);
    const startButton = getByText(/START/i).closest("button");
    const stopButton = getByText(/STOP/i).closest("button");
    const resetButton = getByText(/RESET/i).closest("button");
    if (startButton !== null)
        startButton.click();
    if (resetButton !== null) {
        resetButton.click();
        expect(stopButton).toBeDisabled();
        expect(startButton).toBeEnabled();
    }
});

test('reset chronometer 2', () => {
    const {getByText} = render(<Chrono/>);
    const startButton = getByText(/START/i).closest("button");
    const stopButton = getByText(/STOP/i).closest("button");
    const resetButton = getByText(/RESET/i).closest("button");
    if (resetButton !== null) {
        resetButton.click();
        expect(stopButton).toBeDisabled();
        expect(startButton).toBeEnabled();
    }
});

test('reset chronometer 3', () => {
    const {getByText} = render(<Chrono/>);
    const startButton = getByText(/START/i).closest("button");
    const stopButton = getByText(/STOP/i).closest("button");
    const resetButton = getByText(/RESET/i).closest("button");
    if (startButton !== null)
        startButton.click();
    if (stopButton !== null)
        stopButton.click();
    if (resetButton !== null) {
        resetButton.click();
        expect(stopButton).toBeDisabled();
        expect(startButton).toBeEnabled();
    }
});